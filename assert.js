function assert(message, expected, actual) {
    var $assert = $("#assert");
    if ($assert.length === 0) {
        $assert = $('<ul id="assert"></ul>');
        $("body").append($assert);
    }

    var $message = $('<span class="message">' + message + '</span>');
    var $li = $('<li></li>');
    if (expected === actual) {
        $li.addClass("success").append($message);
    } else {
        var $result = $('<span class="message">想定している値: [' + expected + ']、 実際の値: [' + actual + ']</span>');
        $li.addClass("error").append($message).append($result);
    }
    $assert.append($li);
}
